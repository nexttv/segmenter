<%text><?xml version="1.0" encoding="utf-8"?>
</%text>
<SmoothStreamingMedia Duration="${segments[0][-1]['time'] - segments[0][0]['time']}" MajorVersion="2" MinorVersion="1" TimeScale="10000000">

%for tid, t in slice_['tracks'].items():

    <StreamIndex
        Chunks="${len(segments[tid])}"
        Name="track${tid}"
        QualityLevels="${len(t['qualities'])}"
        Type="${t['media_type']}"
        Url="QualityLevels({bitrate})/Fragments(track${tid}={start time})"
    >

    % for q in t['qualities'].values():
        <QualityLevel
            Bitrate="${q['bitrate']['average']}"
            CodecPrivateData="${q['codec_private_data']}"
            FourCC="${FOURCC[q['codec']]}"
            Index="${loop.index}"
##
        % if t['media_type'] == 'video':
            MaxHeight="${q['height']}"
            MaxWidth="${q['width']}"
##
        % elif t['media_type'] == 'audio':
            AudioTag="${AUDIO_TAGS[q['codec']]}"
            BitsPerSample="${q['sample_bit_depth']}"
            Channels="${q['channels']}"
            PacketSize="1"
            SamplingRate="${q['sampling_rate']}"
##
        % endif
        />
##
    % endfor

    % for s in segments[tid]:
        <c t="${s['time']}"/>
    % endfor

    </StreamIndex>
% endfor

</SmoothStreamingMedia>
