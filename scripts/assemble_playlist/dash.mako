<?xml version="1.0"?>
<MPD mediaPresentationDuration="PT0H1M0.00S" minBufferTime="PT1.5S" profiles="urn:mpeg:dash:profile:isoff-live:2011" type="static" xmlns="urn:mpeg:dash:schema:mpd:2011">
    <ProgramInformation moreInformationURL="http://nexttv.rocks">
        <Title>Title</Title>
        <Copyright>Next TV (c) 2015</Copyright>
    </ProgramInformation>
    <Period duration="PT0H1M0.00S" start="PT0S">
%for tid, t in slice_['tracks'].items():
        <AdaptationSet
            bitstreamSwitching="true"
            segmentAlignment="true"
##
                % if t['media_type'] == 'video':
            maxFrameRate="25"
            maxHeight="1080"
            maxWidth="1920"
            par="16:9"
##
                % endif
        >

            <SegmentTemplate
                duration="20000000"
                initialization="segs/$RepresentationID$-init.m4f"
                media="segs/$RepresentationID$-$Number$.m4f"
                startNumber="0"
                timescale="10000000"
            />

            % for q in t['qualities'].values():
            <Representation
                bandwidth="${q['bitrate']['average']}"
                id="${tid}-${q['quality_id']}"
                startWithSAP="1"
##
                % if t['media_type'] == 'video':
                codecs="${q['codec_ext']}"
                mimeType="video/mp4"
                frameRate="25"
                height="${q['height']}"
                width="${q['width']}"
                sar="1:1"
##
                % elif t['media_type'] == 'audio':
                mimeType="audio/mp4"
                audioSamplingRate="${q['sampling_rate']}"
                codecs="mp4a.40.2"
##
                % endif
                />
            % endfor

            % if t['media_type'] == 'video':
            <ContentComponent contentType="video" id="1"/>
            % elif t['media_type'] == 'audio':
            <AudioChannelConfiguration schemeIdUri="urn:mpeg:dash:23003:3:audio_channel_configuration:2011" value="2"/>
            <ContentComponent contentType="audio" id="1"/>
            % endif

        </AdaptationSet>
% endfor
    </Period>
</MPD>
