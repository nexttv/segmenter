
#include <bitstream/mpeg/pes.h>
#include "PesPacket.h"

PesPacket::PesPacket(uint16_t pid, bool ra, std::vector<uint8_t> && data)
:
	_pid(pid),
	_random_access(ra),
	_data(std::move(data))
{

}

uint16_t PesPacket::pid() const
{
	return _pid;
}

uint64_t PesPacket::dts() const
{
	if(pes_has_dts(_data.data()))
	{
		return pes_get_dts(_data.data());
	}

	return pts();
}

uint64_t PesPacket::pts() const
{
	if(!pes_has_pts(_data.data()))
	{
		throw std::runtime_error("no pts in pes packet");
	}

	return pes_get_pts(_data.data());
}

bool PesPacket::random_access() const
{
	return _random_access;
}

const std::vector<uint8_t> & PesPacket::data() const
{
	return _data;
}
