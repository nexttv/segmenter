
#ifndef MPEG_TS_DEMUXER_H
#define MPEG_TS_DEMUXER_H

#include <deque>
#include <map>
#include <AccessUnit.h>
#include <Demuxer.h>
#include <bitstream/mpeg/psi.h>
#include <bytestream/ByteReader.h>
#include <mpegts/PesPacket.h>
#include "PesBuilder.h"

class MpegTsDemuxer : public Demuxer
{
	enum class State
	{
		BUILD_PAT,
		BUILD_PMT,
		BUILD_AU,
	};

	ByteReaderPtr _bs;
	std::deque<AccessUnitPtr> _au_list;
	PSI_TABLE_DECLARE(_pat);
	std::map<uint16_t, PesBuilderPtr> _pes_builders;
	std::map<uint16_t, uint8_t> _pid_to_stream;
	std::vector<Stream> _streams;
	State _state;
	std::shared_ptr<spdlog::logger> _log;
	std::map<State, std::function<void(uint8_t *)>> _packet_handler;
	int _input_index;
	TimeValue _last_dts;

	bool _consume_some_data();
	void _consume_packet(uint8_t pkt[]);
	void _split_pes(PesPacketPtr);
	void _build_pat(uint8_t pkt[]);
	void _build_pmt(uint8_t pkt[]);
	void _build_au(uint8_t pkt[]);

	void _split_null(const Stream & s, PesPacketPtr pes);
	void _split_aac(const Stream & s, PesPacketPtr pes);
	void _split_ac3(const Stream & s, PesPacketPtr pes);
public:
	MpegTsDemuxer(ByteReaderPtr bs, int input_index);
	~MpegTsDemuxer();

	void probe() override;
	AccessUnitPtr demux() override;
	bool initialized() const override;
	bool eof() const override;
	const std::vector<Stream> & streams() const override;
	TimeValue last_dts() override;
};

#endif
