#ifndef OPTIONAL_H
#define OPTIONAL_H

#include <enforce.h>

template<typename T>
class Optional
{
	T _v;
	bool _set = false;

public:

	Optional()
	{
		_set = false;
	}

	Optional(const T & v)
	{
		_v = v;
		_set = true;
	}

	Optional(const Optional<T> & o)
	{
		if(o._set)
		{
			_v = o._v;
		}

		_set = o._set;
	}

	Optional<T> & operator=(const Optional<T> & o)
	{
		if(o._set)
		{
			_v = o._v;
		}

		_set = o._set;

		return *this;
	}

	bool is_set() const
	{
		return _set;
	}

	operator bool() const
	{
		return _set;
	}

	T& operator->()
	{
		ENFORCE(_set);

		return _v;
	}

	const T& operator->() const
	{
		ENFORCE(_set);

		return _v;
	}

	T& operator*()
	{
		ENFORCE(_set);

		return _v;
	}

	const T& operator*() const
	{
		ENFORCE(_set);

		return _v;
	}
};

#endif
