
#pragma once

#include <memory>
#include <map>
#include <AccessUnit.h>

class Filter
{
public:
	virtual AccessUnitPtr filter(AccessUnitPtr au) = 0;
};

typedef std::shared_ptr<Filter> FilterPtr;

class FilterFactory
{
	std::map<CodecId, FilterPtr> _filters;
	FilterPtr _null_filter;
public:

	FilterFactory();

	FilterPtr find(CodecId codec_id);
	AccessUnitPtr filter(AccessUnitPtr);
};

class NullFilter : public Filter
{
	AccessUnitPtr filter(AccessUnitPtr au) override;
};

class AacFilter : public Filter
{
	AccessUnitPtr filter(AccessUnitPtr au) override;
};

class Ac3Filter : public Filter
{
	AccessUnitPtr filter(AccessUnitPtr au) override;
};

class H264Filter : public Filter
{
	AccessUnitPtr filter(AccessUnitPtr au) override;
};

