
#include <json11.hpp>
#include <timings.h>
#include <stream.h>
#include <timings.h>
#include <io/File.h>
#include <bento4/Ap4.h>
#include <spdlog/spdlog.h>
#include <segmenter/filter.h>
#include "SegmentWriter.h"

using json11::Json;

std::string gen_random_string(const int len)
{
	static const char alphanum[] =
		"0123456789"
		// "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

	std::string s;
	s.resize(len);

	for (int i = 0; i < len; ++i)
	{
		s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
	}

	return std::move(s);
}

SegmentWriter::SegmentWriter(fs::path outdir, std::string resource_id, bool absolute_time)
{
	_log = spdlog::stdout_logger_mt("segment writer");
	_outdir = outdir;
	_resource_id = resource_id;

	if(absolute_time)
	{
		_start_time = TimeValue(time(0), 1);
	}
	else
	{
		_start_time = TimeValue(0, 1);
	}

	_slice_id = fmt::format("{}:{}", _resource_id, rescale_int(_start_time, 1));
}

void SegmentWriter::write(MultiTrackSegmentPtr mts)
{
	if(!_headers_written)
	{
		_write_slice_headers(mts);
	}

	_write_segments(mts);
}

void SegmentWriter::_write_segments(MultiTrackSegmentPtr mts)
{
	FileList file_list;

	for(auto ms: mts->segments())
	{
		auto fl = _write_segments(ms);
		file_list.insert(file_list.end(), fl.begin(), fl.end());
	}

	_write_push_file(file_list);
}

SegmentWriter::FileList SegmentWriter::_write_segments(MultiSegmentPtr ms)
{
	FileList ret_fl;

	for(auto monoseg: ms->segments())
	{
		auto fl = _write_segments(monoseg);
		ret_fl.insert(ret_fl.end(), fl.begin(), fl.end());
	}

	return ret_fl;
}

MonoSegment SegmentWriter::_filter_mono_segment(const MonoSegment & s)
{
	std::vector<AccessUnitPtr> filtered_samples;
	for(auto & au : s.samples)
	{
		auto fau = _filters.filter(au);
		filtered_samples.push_back(fau);
	}

	return MonoSegment(s.index, s.time, s.duration, std::move(filtered_samples));
}

SegmentWriter::FileList SegmentWriter::_write_segments(const MonoSegment & s)
{
	// auto sid = fmt::format("{:09}-{}-{}-{}", s.index, s.stream().index, s.stream().input, gen_random_string(16));
	auto sid = gen_random_string(16);

	auto filtered_segment = _filter_mono_segment(s);

	_write_mp4_fragment(filtered_segment, sid);
	_write_segment_meta(filtered_segment, sid);

	return {sid};
}

void SegmentWriter::_write_slice_headers(MultiTrackSegmentPtr mts)
{
	_parse_info = _parser.parse(mts);

	_write_slice_info();
	_write_init_segments(mts);

	_headers_written = true;
}

void SegmentWriter::_write_init_segments(MultiTrackSegmentPtr mts)
{
	FileList fl;

	for(size_t tidx = 0; tidx < _parse_info.size(); ++tidx)
	{
		for(size_t qidx = 0; qidx < _parse_info[tidx].size(); ++qidx)
		{
			auto fl1 = _write_init_segment(_parse_info[tidx][qidx]);
			fl.insert(fl.end(), fl1.begin(), fl1.end());
		}
	}

	_write_push_file(fl);
}

AP4_FtypAtom * create_ftyp()
{
	std::vector<uint32_t> compatible_brands =
	{
		AP4_FILE_BRAND_ISO6,
		AP4_ATOM_TYPE('d','a','s','h'),
	};

	auto ftyp = new AP4_FtypAtom(
		AP4_FILE_BRAND_ISO2,
		0,
		compatible_brands.data(),
		compatible_brands.size()
	);

	return ftyp;
}

AP4_ContainerAtom * create_mvex(int tid)
{
	auto mvex = new AP4_ContainerAtom(AP4_ATOM_TYPE_MVEX);

	auto trex = new AP4_TrexAtom(
		tid,
		1,
		0,
		0,
		0
	);

	mvex->AddChild(trex);

	return mvex;
}

AP4_Track * create_track_h264(H264ParseInfoPtr pi)
{
	AP4_Array<AP4_DataBuffer> sps;
	AP4_Array<AP4_DataBuffer> pps;

	for(auto & x : pi->sps_list)
	{
		AP4_DataBuffer buf(x.data(), x.size());
		sps.Append(buf);
	}

	for(auto & x : pi->pps_list)
	{
		AP4_DataBuffer buf(x.data(), x.size());
		pps.Append(buf);
	}

	auto sample_description = new AP4_AvcSampleDescription(
		AP4_ATOM_TYPE('a','v','c','1'),
		pi->width,
		pi->height,
		0x0018,
		"nexttv segmenter",
		pi->profile,
		pi->level,
		pi->profile_compatibility,
		4,
		sps,
		pps
	);

	auto sample_table = new AP4_SyntheticSampleTable;
	sample_table->AddSampleDescription(sample_description);

	auto track = new AP4_Track(
		AP4_Track::TYPE_VIDEO,
		sample_table,
		pi->stream.index + 1,
		SEGMENT_TIME_SCALE,
		0,
		SEGMENT_TIME_SCALE,
		0,
		"UNK",
		pi->width << 16,
		pi->height << 16
	);

	return track;
}

void make_dsi(unsigned int sampling_frequency_index, unsigned int channel_configuration, unsigned char* dsi)
{
	unsigned int object_type = 2; // AAC LC by default
	dsi[0] = (object_type<<3) | (sampling_frequency_index>>1);
	dsi[1] = ((sampling_frequency_index&1)<<7) | (channel_configuration<<3);
}

AP4_Track * create_track_aac(AacParseInfoPtr pi)
{
	AP4_DataBuffer dsi;

	unsigned char aac_dsi[2];
	make_dsi(pi->sampling_frequency_index, pi->channels_configuration, aac_dsi);
	dsi.SetData(aac_dsi, 2);

	auto sd = new AP4_MpegAudioSampleDescription(
		AP4_OTI_MPEG4_AUDIO,				// object type
		pi->sampling_rate,
		16,									// sample size
		pi->channels_configuration,
		&dsi,								// decoder info
		6144,								// buffer size
		pi->max_bitrate,					// max bitrate
		pi->average_bitrate					// average bitrate
	);

	auto sample_table = new AP4_SyntheticSampleTable;
	sample_table->AddSampleDescription(sd);

	auto track = new AP4_Track(
		AP4_Track::TYPE_AUDIO,
		sample_table,
		pi->stream.index + 1,
		SEGMENT_TIME_SCALE,
		0,
		SEGMENT_TIME_SCALE,
		0,
		"UNK",
		0,
		0
	);

	return track;
}

AP4_Track * create_track(ParseInfoPtr pi)
{
	AP4_Track * t = nullptr;

	switch(pi->stream.codec_id)
	{
		case CodecId::H264:
		{
			auto pi_ = std::dynamic_pointer_cast<H264ParseInfo>(pi);
			t = create_track_h264(pi_);
			break;
		}
		case CodecId::Aac:
		{
			auto pi_ = std::dynamic_pointer_cast<AacParseInfo>(pi);
			t = create_track_aac(pi_);
			break;
		}
		default:
			throw std::runtime_error(fmt::format("unknown or unsupported codec {}", pi->stream.codec_id));
	}

	return t;
}

SegmentWriter::FileList SegmentWriter::_write_init_segment(ParseInfoPtr pi)
{
	auto sid = gen_random_string(16);

	_write_init_segment_data(pi, sid);
	_write_init_segment_meta(pi, sid);

	return {sid};
}

void SegmentWriter::_write_init_segment_meta(ParseInfoPtr pi, std::string sid)
{
	auto quality_id = pi->average_bitrate;

	Json meta = Json::object {
		{"segment_id", sid},
		{"resource_id", _resource_id},
		{"slice_id", _slice_id},
		{"track_id", pi->stream.index},
		{"quality_id", quality_id},
		{"type", "init"},
	};

	auto meta_str = meta.dump();

	auto file = File(_outdir / (sid + ".meta"), "wb");
	file.writeln(meta_str);
}

void SegmentWriter::_write_init_segment_data(ParseInfoPtr pi, std::string sid)
{
	std::vector<AP4_Atom*> atoms;

	atoms.push_back(create_ftyp());

	auto movie = std::make_shared<AP4_Movie>(SEGMENT_TIME_SCALE);

	auto track = create_track(pi);

	movie->AddTrack(track);

	auto mvex = create_mvex(pi->stream.index + 1);
	movie->GetMoovAtom()->AddChild(mvex);

	atoms.push_back(movie->GetMoovAtom());

	auto path = _outdir / (sid + ".data");
	auto bs = new AP4_FileByteStream(path.c_str(), AP4_FileByteStream::STREAM_MODE_WRITE);

	for(auto a: atoms)
	{
		a->Write(*bs);
	}

	bs->Flush();
	AP4_RELEASE(bs);

	delete atoms[0];
}

void SegmentWriter::_write_slice_info()
{
	auto track_list = _prepare_track_list();

	Json si = Json::object {
		{"resource_id", _resource_id},
		{"slice_id", _slice_id},
		{"start_time", (int64_t) rescale_int(_start_time, 1)},
		{"track_list", track_list},
	};

	auto file = File(_outdir / fmt::format("{}.slice", _slice_id), "wb");
	file.writeln(si.dump());
}

Json SegmentWriter::_prepare_track_list()
{
	auto track_list = Json::array();

	for(size_t tid = 0; tid < _parse_info.size(); ++tid)
	{
		auto media_type = _parse_info[tid][0]->stream.media_type();

		auto quality_list = _prepare_quality_list(tid);

		auto track_info = Json::object {
			{"track_id", tid},
			{"media_type", std::to_string(media_type)},
			{"lang", _parse_info[tid][0]->lang},
			{"quality_list", quality_list},
		};

		track_list.push_back(track_info);
	}

	auto j = Json(std::move(track_list));
	return std::move(j);
}

Json SegmentWriter::_prepare_quality_list(size_t tid)
{
	auto qlist = std::vector<Json>();

	for(size_t qid=0; qid < _parse_info[tid].size(); ++qid)
	{
		auto & q = _parse_info[tid][qid];
		auto j = q->quality_info();
		qlist.push_back(std::move(j));
	}

	auto ret = Json(std::move(qlist));
	return std::move(ret);
}

void SegmentWriter::_write_push_file(const FileList & fl)
{
	auto name = gen_random_string(16) + ".push";

	auto file = File(_outdir / name, "wb");

	for(auto x: fl)
	{
		file.writeln(x);
	}
}

AP4_ContainerAtom * SegmentWriter::create_traf(const MonoSegment & s)
{
	auto traf = new AP4_ContainerAtom(AP4_ATOM_TYPE_TRAF);

	unsigned int tfhd_flags = AP4_TFHD_FLAG_DEFAULT_BASE_IS_MOOF | AP4_TFHD_FLAG_DEFAULT_SAMPLE_FLAGS_PRESENT;

	AP4_TfhdAtom* tfhd = new AP4_TfhdAtom(tfhd_flags,
										  s.stream().index + 1,
										  0,
										  /*sample_desc_index+*/1,
										  0,
										  0,
										  0);

	if (tfhd_flags & AP4_TFHD_FLAG_DEFAULT_SAMPLE_FLAGS_PRESENT)
	{
		tfhd->SetDefaultSampleFlags(0xc0);
	}

	tfhd->SetFlags(tfhd_flags);
	traf->AddChild(tfhd);

	// base media decode time field.
	// auto tfdt = new AP4_TfdtAtom(1, rescale_int(s.time, SEGMENT_TIME_SCALE));
	// traf->AddChild(tfdt);

	int trun_flags = 0
		| AP4_TRUN_FLAG_DATA_OFFSET_PRESENT
		| AP4_TRUN_FLAG_SAMPLE_DURATION_PRESENT
		| AP4_TRUN_FLAG_SAMPLE_SIZE_PRESENT;

	int first_sample_flags = 0;

	if (s.samples[0]->random_access())
	{
		trun_flags |= AP4_TRUN_FLAG_FIRST_SAMPLE_FLAGS_PRESENT;
		first_sample_flags = 0x2000000;
		// sample_depends_on=2 (I frame)
	}

	AP4_Array<AP4_TrunAtom::Entry> trun_entries;

	for(auto au: s.samples)
	{
		auto cts_delta = rescale_int(au->pts() - au->dts(), SEGMENT_TIME_SCALE);

		if (cts_delta > 0)
		{
			trun_flags |= AP4_TRUN_FLAG_SAMPLE_COMPOSITION_TIME_OFFSET_PRESENT;
		}

		AP4_TrunAtom::Entry trun_entry;
		trun_entry.sample_duration                = rescale_int(*au->duration(), SEGMENT_TIME_SCALE);
		trun_entry.sample_size                    = au->data().size();
		trun_entry.sample_composition_time_offset = cts_delta;

		trun_entries.Append(trun_entry);
	}


	AP4_TrunAtom* trun = new AP4_TrunAtom(trun_flags, 0, first_sample_flags);
	trun->SetEntries(trun_entries);

	traf->AddChild(trun);

	return traf;
}

void SegmentWriter::_write_mp4_fragment(const MonoSegment & s, const std::string & sid)
{
	auto moof = std::make_shared<AP4_ContainerAtom>(AP4_ATOM_TYPE_MOOF);
	AP4_MfhdAtom* mfhd = new AP4_MfhdAtom(-1);
	moof->AddChild(mfhd);

	std::vector<uint8_t> mdat;
	for(auto & au : s.samples)
	{
		mdat.insert(mdat.end(), au->data().begin(), au->data().end());
	}

	auto traf = create_traf(s);
	moof->AddChild(traf);
	mfhd->SetSequenceNumber(s.index);

	auto trun = (AP4_TrunAtom*) traf->GetChild(AP4_ATOM_TYPE_TRUN);
	trun->SetDataOffset(AP4_ATOM_HEADER_SIZE + moof->GetSize());

	// serialize data

	AP4_DataBuffer buf;
	AP4_ByteStream * bs = new AP4_MemoryByteStream(buf);

	AP4_Result result = 0;

	result = moof->Write(*bs);
	ENFORCE(AP4_SUCCEEDED(result));

	bs->WriteUI32(mdat.size() + AP4_ATOM_HEADER_SIZE);
	bs->WriteUI32(AP4_ATOM_TYPE_MDAT);

	auto file = File(_outdir / (sid + ".data"), "wb");

	file.write(buf.GetData(), buf.GetDataSize());
	AP4_RELEASE(bs);
	bs = nullptr;
	file.write(mdat.data(), mdat.size());
}

void SegmentWriter::_write_segment_meta(const MonoSegment & s, const std::string & sid)
{
	auto quality_id = _parse_info[s.stream().index][s.stream().input]->average_bitrate;

	Json meta = Json::object {
		{"segment_id", sid},
		{"resource_id", _resource_id},
		{"slice_id", _slice_id},
		{"track_id", s.stream().index},
		{"quality_id", quality_id},
		{"index", s.index},
		{"time", rescale_int(s.time + _start_time, SEGMENT_TIME_SCALE)},
		{"duration", rescale_int(s.duration, SEGMENT_TIME_SCALE)},
		{"type", "data"},
	};

	auto meta_str = meta.dump();

	auto file = File(_outdir / (sid + ".meta"), "wb");
	file.writeln(meta_str);
}

