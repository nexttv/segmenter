
#ifndef __SEGMENT_WRITER_H__
#define __SEGMENT_WRITER_H__

#include <memory>
#include <vector>
#include <string>
#include <json11.hpp>
#include <parser/Parser.h>
#include <boost/filesystem.hpp>
#include <segmenter/segments.h>
#include <segmenter/filter.h>
#include <bento4/Ap4.h>

namespace fs = boost::filesystem;

const TimeScale SEGMENT_TIME_SCALE = 10000000;

class SegmentWriter
{
	typedef std::vector<std::string> FileList;

	fs::path _outdir;
	std::string _resource_id;
	TimeValue _start_time;
	bool _headers_written = false;
	std::string _slice_id;
	Parser _parser;
	FullParseInfo _parse_info;
	std::shared_ptr<spdlog::logger> _log;
	FilterFactory _filters;

	MonoSegment _filter_mono_segment(const MonoSegment & s);

	void _write_slice_headers(MultiTrackSegmentPtr mts);
	void _write_slice_info();
	json11::Json _prepare_track_list();
	json11::Json _prepare_quality_list(size_t tid);

	void _write_init_segments(MultiTrackSegmentPtr ms);
	FileList _write_init_segments(MultiSegmentPtr ms);
	FileList _write_init_segment(ParseInfoPtr pi);
	void _write_init_segment_data(ParseInfoPtr pi, std::string sid);
	void _write_init_segment_meta(ParseInfoPtr pi, std::string sid);

	void _write_segments(MultiTrackSegmentPtr ms);
	FileList _write_segments(MultiSegmentPtr ms);
	FileList _write_segments(const MonoSegment & ms);

	void _write_mp4_fragment(const MonoSegment & ms, const std::string & sid);
	void _write_segment_meta(const MonoSegment & ms, const std::string & sid);

	void _write_push_file(const FileList & fl);

	AP4_ContainerAtom * create_traf(const MonoSegment & s);
public:

	SegmentWriter(fs::path outdir, std::string resource_id, bool absolute_time);

	void write(MultiTrackSegmentPtr mts);
};

typedef std::shared_ptr<SegmentWriter> SegmentWriterPtr;

#endif /*__SEGMENT_WRITER_H__*/
