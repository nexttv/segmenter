
#include <bento4/Ap4.h>
#include <bento4/Ap4AdtsParser.h>
#include <bitstream/mpeg/h264.h>
#include "filter.h"

FilterFactory::FilterFactory()
{
	_filters[CodecId::H264].reset(new H264Filter);
	_filters[CodecId::Aac].reset(new AacFilter);
	_filters[CodecId::Ac3].reset(new Ac3Filter);

	_null_filter.reset(new NullFilter);
}

FilterPtr FilterFactory::find(CodecId codec_id)
{
	auto it = _filters.find(codec_id);

	if(it != _filters.end())
	{
		return it->second;
	}

	return _null_filter;
}

AccessUnitPtr FilterFactory::filter(AccessUnitPtr au)
{
	auto filter = find(au->stream().codec_id);
	return filter->filter(au);
}

AccessUnitPtr NullFilter::filter(AccessUnitPtr au)
{
	return au;
}

AccessUnitPtr AacFilter::filter(AccessUnitPtr au)
{
	AP4_AdtsParser parser;
	AP4_AacFrame frame;

	AP4_Size bytes_consumed = au->data().size();

	auto r = parser.Feed(au->data().data(), &bytes_consumed, AP4_BITSTREAM_FLAG_EOS);

	ENFORCE(r == 0 &&  bytes_consumed == au->data().size(), fmt::format("failed to feed aac adts data to parser in full: only {} out of {} bytes feeded", bytes_consumed, au->data().size()));

	auto result = parser.FindFrame(frame);

	ENFORCE(result == 0, "failed to find adts frame");

	AP4_MemoryByteStream* sample_data = new AP4_MemoryByteStream(frame.m_Info.m_FrameLength);
	frame.m_Source->ReadBytes(sample_data->UseData(), frame.m_Info.m_FrameLength);

	std::vector<uint8_t> aac_raw_data(sample_data->GetData(), sample_data->GetData() + sample_data->GetDataSize());

	auto filtered_au = std::make_shared<AccessUnit>(
						au->stream(),
						au->random_access(),
						au->dts(),
						au->pts(),
						au->duration(),
						std::move(aac_raw_data)
	);

	return filtered_au;
}

// removes Annex B start codes and SPS,PPS,AUD nal units
AccessUnitPtr H264Filter::filter(AccessUnitPtr au)
{

	AP4_AvcNalParser parser;

	size_t offset = 0;

	// printf("au codec: %d data: %s\n", au->stream().codec_id, dump_bytes(au->data().begin(), au->data().end()).c_str());

	AP4_MemoryByteStream * filtered_data = new AP4_MemoryByteStream;

	while(offset < au->data().size())
	{
		AP4_Size bytes_consumed = 0;

		const AP4_DataBuffer * nalubuf = NULL;
		auto res = parser.Feed(au->data().data() + offset, au->data().size() - offset, bytes_consumed, nalubuf, true);

		ENFORCE(res == 0 && bytes_consumed > 0 && nalubuf != nullptr);

		int nalutype = nalubuf->GetData()[0] & 0x1f;

		// auto s = dump_bytes(nalubuf->GetData(), nalubuf->GetData() + nalubuf->GetDataSize());
		// printf("nalutype: %d nalu: %s\n", nalutype, s.c_str());

		switch(nalutype)
		{
			case H264NAL_TYPE_AUD:
			case H264NAL_TYPE_SEI:
			case H264NAL_TYPE_SPS:
			case H264NAL_TYPE_PPS:
				// skip
				break;
			default:
			{
				filtered_data->WriteUI32(nalubuf->GetDataSize());
				AP4_Size nwritten = 0;
				filtered_data->WritePartial(nalubuf->GetData(), nalubuf->GetDataSize(), nwritten);
				ENFORCE(nwritten == nalubuf->GetDataSize());
			}
		}

		offset += bytes_consumed;
	}

	std::vector<uint8_t> filtered_data_vec(filtered_data->GetData(), filtered_data->GetData() + filtered_data->GetDataSize());

	auto filtered_au = std::make_shared<AccessUnit>(
						au->stream(),
						au->random_access(),
						au->dts(),
						au->pts(),
						au->duration(),
						std::move(filtered_data_vec)
	);

	AP4_RELEASE(filtered_data);

	return filtered_au;
}

AccessUnitPtr Ac3Filter::filter(AccessUnitPtr au)
{
	NOT_IMPLEMENTED();
}
