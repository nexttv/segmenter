
#ifndef __MULTI_SEGMENT_BUILDER_H__
#define __MULTI_SEGMENT_BUILDER_H__

#include <memory>
#include <timings.h>
#include <AccessUnit.h>
#include <segmenter/Intersector.h>
#include <segmenter/segments.h>

class MultiSegmentBuilder
{
	std::vector<std::vector<AccessUnitPtr>> _samples;
	Intersector<TimeValue> _intersector;
	int _stream_index;
	std::shared_ptr<spdlog::logger> _log;
	bool _first_ra_found = false;
public:

	MultiSegmentBuilder(const std::vector<Stream> & streams);

	bool add(AccessUnitPtr au);
	MultiSegmentPtr build(TimeValue sync_point, uint64_t index);
	TimeValue last_sync_point() const;
	const std::vector<TimeValue> & sync_points() const;
};

typedef std::shared_ptr<MultiSegmentBuilder> MultiSegmentBuilderPtr;

#endif /*__MULTI_SEGMENT_BUILDER_H__*/
