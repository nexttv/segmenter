
#ifndef __MONO_SEGMENT_H__
#define __MONO_SEGMENT_H__

#include <vector>
#include <AccessUnit.h>
#include <timings.h>

class MonoSegment
{
public:
	MonoSegment(uint64_t index, TimeValue time, Duration duration, std::vector<AccessUnitPtr> && samples_)
	:
		index(index),
		time(time),
		duration(duration),
		samples(std::move(samples_))
	{
#ifdef DEBUG
		validate();
#endif

		for(size_t i = 0; i+1 < samples.size(); ++i)
		{
			samples[i]->set_duration(samples[i+1]->dts() - samples[i]->dts());
		}

		samples.back()->set_duration(time + duration - samples.back()->dts());
	}

	const Stream & stream() const
	{
		return samples[0]->stream();
	}

	void validate()
	{
		auto au0 = samples[0];

		ENFORCE(au0->random_access());

		for(auto & au: samples)
		{
			ENFORCE(au->stream().index == au0->stream().index);
			ENFORCE(au->stream().input == au0->stream().input);
			ENFORCE(au->dts() >= time);
			ENFORCE(au->dts() <= time + duration);
		}
	}

	uint64_t index;
	TimeValue time;
	Duration duration;
	std::vector<AccessUnitPtr> samples;
};

#endif /*__MONO_SEGMENT_H__*/
