
#ifndef BYTE_READER_H
#define BYTE_READER_H

#include <memory>

class ByteReader
{
public:
	virtual size_t read(uint8_t* buf, size_t size) = 0;
	// virtual bool would_block() const = 0;
	virtual bool eof() const = 0;

	virtual ~ByteReader(){}
};

typedef std::shared_ptr<ByteReader> ByteReaderPtr;

#endif

