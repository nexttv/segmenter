
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <spdlog/spdlog.h>
#include <unistd.h>
#include <utils.h>
#include <enforce.h>
#include "UdpByteReader.h"

UdpByteReader::UdpByteReader(std::string group, uint16_t port)
{
	_fd = socket(AF_INET, SOCK_DGRAM, 0);
	_log = spdlog::stdout_logger_mt(fmt::format("udp:{}:{}", group, port));

	if(_fd < 0)
	{
		throw std::runtime_error("failed to create udp socket");
	}

	struct sockaddr_in s_addr;

	s_addr.sin_family = AF_INET;
	s_addr.sin_addr.s_addr = inet_addr(group.c_str());

	s_addr.sin_port = htons(port);

	if(bind(_fd, (sockaddr*) &s_addr, sizeof(s_addr)) < 0)
	{
		throw std::runtime_error(fmt::format("failed to bind to {}:{}", group, port));
	}

	// join multicast group
	auto  inet_address = inet_addr(group.c_str());

	ip_mreq mreq;
	mreq.imr_multiaddr.s_addr = inet_address;
	mreq.imr_interface.s_addr = INADDR_ANY;

	if (setsockopt(_fd, IPPROTO_IP, IP_ADD_MEMBERSHIP, &mreq, sizeof(struct ip_mreq)) < 0 )
	{
		throw std::runtime_error(fmt::format("failed to join multicast group {}:{}", group, port));
	}

	// set non blocking
	long dontblock = 1;
	int rc = ioctl(_fd, FIONBIO, (char *)&dontblock);
	if(rc)
	{
		throw std::runtime_error("failed to set socket non blocking");
	}

	_last_packet_time = 0;
}

UdpByteReader::~UdpByteReader()
{
	close(_fd);
}

size_t UdpByteReader::read(uint8_t * buf, size_t sz)
{

	if(_buffer.size() < sz)
	{
		_try_to_fill_buffer();
	}

	auto copy_sz = std::min(sz, _buffer.size());
	std::copy(_buffer.begin(), _buffer.begin() + copy_sz, buf);
	_buffer.erase(_buffer.begin(), _buffer.begin() + copy_sz);

	return copy_sz;
}

void UdpByteReader::_try_to_fill_buffer()
{
	uint8_t buf[1500];

	auto nread = ::recv(_fd, buf, sizeof(buf), 0);

	if(nread == 0)
	{
		return;
	}

	if(nread < 0)
	{
		if(errno != EWOULDBLOCK && errno != EAGAIN)
		{
			_log->warn("recv returned error {}: {}", nread, strerror(errno));
		}
		return;
	}
	ENFORCE(nread % 188 == 0);

	std::copy(buf, buf + nread, std::back_inserter(_buffer));
	_last_packet_time = time(0);
}

bool UdpByteReader::eof() const
{
	return _last_packet_time != 0 && time(0) - _last_packet_time > 2;
}
