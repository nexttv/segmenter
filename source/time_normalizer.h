
#pragma once

#include <queue>
#include <map>
#include <AccessUnit.h>

class TimeNormalizer
{
	typedef std::queue<AccessUnitPtr> SamplesQueue;
	std::map<std::pair<int,int>, SamplesQueue> _samples;
	Optional<Duration> _first_dts;

	void _calc_first_dts();
public:

	TimeNormalizer(const std::vector<Stream> & streams);

	AccessUnitPtr normalize(AccessUnitPtr au);
};
