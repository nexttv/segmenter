
#ifndef ACCESS_UNIT_H
#define ACCESS_UNIT_H

#include <vector>
#include <timings.h>
#include <stream.h>
#include <optional.h>

typedef std::vector<uint8_t> ByteRange;

class AccessUnit
{
	Stream _stream;
	TimeValue _dts;
	TimeValue _pts;
	Optional<Duration> _duration;
	ByteRange _data;
	bool _random_access;
public:

	AccessUnit(const Stream& s, bool ra, TimeValue dts, TimeValue pts, Optional<Duration> duration, const ByteRange & data)
	{
		_stream = s;
		_dts = dts;
		_pts = pts;
		_duration = duration;
		_data = data;
		_random_access = ra;
	}

	AccessUnit(const Stream& s, bool ra, TimeValue dts, TimeValue pts, Optional<Duration> duration, ByteRange && data)
	: _data (std::move(data))
	{
		_stream = s;
		_dts = dts;
		_pts = pts;
		_duration = duration;
		_random_access = ra;
	}

	const Stream& stream() const
	{
		return _stream;
	}

	TimeValue dts() const
	{
		return _dts;
	}

	TimeValue pts() const
	{
		return _pts;
	}

	Optional<Duration> duration() const
	{
		return _duration;
	}

	void shift_time(Duration reverse_diff)
	{
		_dts -= reverse_diff;
		_pts -= reverse_diff;
	}

	void set_duration(const Optional<Duration> & d)
	{
		_duration = d;
	}

	const ByteRange & data() const
	{
		return _data;
	}

	bool random_access() const
	{
		return _random_access;
	}
};

typedef std::shared_ptr<AccessUnit> AccessUnitPtr;

#endif
