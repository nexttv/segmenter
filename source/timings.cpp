
#include "timings.h"

TimeValue rescale(const TimeValue & v, TimeScale scale)
{
	return v / TimeValue(1, scale);
}

uint64_t rescale_int(const TimeValue & v, TimeScale scale)
{
	return boost::rational_cast<uint64_t>(rescale(v, scale));
}

double rescale_double(const TimeValue & v, TimeScale scale)
{
	return boost::rational_cast<double>(rescale(v, scale));
}
