
#ifndef TIME_H
#define TIME_H

#include <stdint.h>
#include <ctime>
#include <boost/rational.hpp>

typedef boost::rational<int64_t> TimeValue;
typedef boost::rational<int64_t> Duration;

typedef uint64_t TimeScale;

uint64_t rescale_int(const TimeValue & v, TimeScale scale);
double rescale_double(const TimeValue & v, TimeScale scale);

#endif
