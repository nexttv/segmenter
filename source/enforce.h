
#ifndef __ENFORCE_H__
#define __ENFORCE_H__

#include <string>
#include <stdexcept>
#include <spdlog/spdlog.h>

class EnforceEx : public std::runtime_error
{
public:
	EnforceEx(const char * cond, const char * file, int line, std::string msg = "")
	: std::runtime_error(fmt::format("enforcement failed ({}): {} @{}:{}", cond, msg, file, line))
	{
	}
};

class NotImplementedEx : public std::runtime_error
{
public:
	NotImplementedEx(const char * func, const char * file, int line)
	: std::runtime_error(fmt::format("not implemented function {} @{}:{}", func, file, line))
	{
	}

};

template<typename Except = std::runtime_error, typename... Args>
void enforce_ex(bool condition, Args... args)
{
	if(!condition)
	{
		throw Except(args...);
	}
}

#define ENFORCE(cond, ...) \
	enforce_ex<EnforceEx>(cond, #cond, __FILE__, __LINE__, ## __VA_ARGS__)

#define NOT_IMPLEMENTED() throw NotImplementedEx(__PRETTY_FUNCTION__, __FILE__, __LINE__);

#endif /*__ENFORCE_H__*/
