
#ifndef STREAM_H
#define STREAM_H

#include <cstdint>
#include <iostream>

enum class CodecId
{
	H264,
	Aac,
	Ac3,
};

enum class MediaType
{
	Video,
	Audio,
};

class Stream
{
public:
	int input;
	int index;
	CodecId codec_id;
	MediaType media_type() const;
};

CodecId stream_id_to_codec_id(uint8_t stream_id);
MediaType codec_to_media_type(CodecId codec_id);

namespace std
{
	string to_string(MediaType mt);
}

std::ostream & operator<<(std::ostream & os, CodecId c);

#endif
