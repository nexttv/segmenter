
#include <algorithm>
#include <signal.h>
#include <segmenter/segmenter.h>
#include <time_normalizer.h>
#include <Demuxer.h>
#include <bytestream/FileByteReader.h>
#include <bytestream/UdpByteReader.h>
#include <mpegts/MpegTsDemuxer.h>
#include <boost/program_options.hpp>
#include <boost/algorithm/string.hpp>
#include <spdlog/spdlog.h>
#include <formatter.h>

auto logger = spdlog::stdout_logger_mt("app");
volatile bool running_flag = true;

ByteReaderPtr create_byte_reader(std::string input)
{
	ByteReaderPtr bs;

	if(input.size() >= 6 && input.substr(0, 6) == "udp://")
	{
		std::vector<std::string> toks;
		auto x = input.substr(6);
		boost::split(toks, x, boost::is_any_of(":"));

		ENFORCE(toks.size() == 2);
		auto ubs = std::make_shared<UdpByteReader>(toks[0], stoi(toks[1]));
		bs = std::dynamic_pointer_cast<ByteReader>(ubs);
	}
	else
	{
		auto fbs = std::make_shared<FileByteReader>(input);
		bs = std::dynamic_pointer_cast<ByteReader>(fbs);
	}

	return bs;
}

std::vector<DemuxerPtr> create_demuxers(std::vector<std::string> inputs)
{
	std::vector<DemuxerPtr> demuxers;

	size_t idx = 0;
	for(auto i : inputs)
	{
		auto bs = create_byte_reader(i);
		auto d = std::make_shared<MpegTsDemuxer>(bs, idx);
		demuxers.push_back(std::dynamic_pointer_cast<Demuxer>(d));
		++idx;
	}

	return std::move(demuxers);
}

void initialize_demuxers(const std::vector<DemuxerPtr> & demuxers)
{
	auto dm_to_init = demuxers.size();
	bool all_alive = true;

	while(running_flag && dm_to_init > 0 && all_alive)
	{
		bool all_would_block = true;

		for(auto & d : demuxers)
		{
			if(d->initialized())
			{
				continue;
			}

			if(!d->initialized())
			{
				d->probe();
			}

			if(d->initialized())
			{
				dm_to_init -= 1;
			}
			else
			{
				all_would_block &= ! d->eof();
			}

			if(d->eof())
			{
				all_alive = false;
			}
		}

		if(all_alive && dm_to_init > 0 && all_would_block)
		{
			usleep(10000);
		}
	}
}

std::map<std::pair<int,int>, Stream> calc_stream_map(const std::vector<DemuxerPtr> & demuxers)
{
	std::map<std::pair<int,int>, Stream> stream_map;

	for(auto d : demuxers)
	{
		for(const auto & st: d->streams())
		{
			auto p = std::make_pair(st.input, st.index);
			auto new_st = st;
			new_st.index = stream_map.size();

			stream_map.insert({p, new_st});
		}
	}

	return stream_map;
}

DemuxerPtr select_input(const std::vector<DemuxerPtr> & demuxers)
{
	TimeValue min_dts = TimeValue(INT64_MAX, 1);
	DemuxerPtr min_d = nullptr;

	for(auto d : demuxers)
	{
		if(d->eof())
		{
			continue;
		}

		if(d->last_dts() < min_dts)
		{
			min_dts = d->last_dts();
			min_d = d;
		}
	}

	return min_d;
}

void segmentize(const std::vector<DemuxerPtr> & demuxers, std::string output_path, const std::map<std::pair<int,int>, Stream> & stream_map, bool absolute_time, std::string resource_id)
{
	std::vector<Stream> streams;
	for(auto d : demuxers)
	{
		auto dstreams = d->streams();
		streams.insert(streams.end(), dstreams.begin(), dstreams.end());
	}

	auto time_normalizer = TimeNormalizer(streams);
	auto segment_writer = std::make_shared<SegmentWriter>(output_path, resource_id, absolute_time);
	auto segmenter = std::make_shared<Segmenter>(segment_writer, streams);

	for(DemuxerPtr current_input = select_input(demuxers);
		running_flag && current_input != nullptr;
		current_input = select_input(demuxers))
	{
		auto au = current_input->demux();
		if(!au)
		{
			SPDLOG_DEBUG(logger, "sleeping: no active inputs");
			usleep(10000);
			continue;
		}


		auto old_dts = au->dts();
		au = time_normalizer.normalize(au);

		if(au)
		{
			SPDLOG_DEBUG(logger, "normalized time: stream: {}:{}: dts: old: {} ({}|{:.2f}) new: {} ({}|{:.2f})",
				au->stream().input,
				au->stream().index,
				old_dts,
				rescale_int(old_dts, SEGMENT_TIME_SCALE),
				rescale_double(old_dts, 1),
				au->dts(),
				rescale_int(au->dts(), SEGMENT_TIME_SCALE),
				rescale_double(au->dts(), 1)
			);
			segmenter->add(au);
		}
	}
}

void terminate(int sig)
{
	logger->info("signal {} received: terminating ...", sig);

	running_flag = false;
}

int main(int argc, char* argv[])
{
	srand(time(0));

	namespace po = boost::program_options;

	auto fmt = std::make_shared<CustomFormatter>();
	spdlog::set_formatter(std::dynamic_pointer_cast<spdlog::formatter>(fmt));

	signal(SIGINT,  &terminate);
	signal(SIGTERM, &terminate);

	po::options_description desc("usage");
	desc.add_options()
	("help,h", "produce help message")
	("force,f", po::value<bool>()->default_value(false), "overwrite output file/dir")
	("absolute-time,a", po::value<bool>()->default_value(false), "use absolute timestamps (starting from current unix timestamp)")
	("resource-id,r", po::value<std::string>()->required(), "resource-id string [a-z0-9_]+")
	("input,i", po::value<std::vector<std::string>>()->value_name("<input>"), "input files (may be specified multiple times)")
	("output,o", po::value<std::vector<std::string>>()->value_name("<output>"), "output dir where segments will be stored")
	("verbose,v", po::value<unsigned>()->value_name("<level>")->default_value(2), "log level to output, 0 - the most detail (trace), 9 - no logs")
	;

	po::positional_options_description p;
	p.add("output", 1);

	po::variables_map vm;
	// po::store(po::parse_command_line(argc, argv, desc), vm);
	po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

	try
	{
		po::notify(vm);
	}
	catch(std::exception & e)
	{
		std::cout << e.what() << std::endl;
		return 1;
	}

	if(vm.count("help") || argc == 1)
	{
		std::cout << desc << std::endl;
		return 0;
	}

	if(vm.count("input") == 0 || vm.count("output") == 0)
	{
		std::cout << "at least one <input> (-i) and <output> should be provided" << std::endl;
		std::cout << desc << std::endl;

		return -1;
	}

	unsigned log_level = vm["verbose"].as<unsigned>();
	log_level = std::min(log_level, 9U);

	spdlog::set_level((spdlog::level::level_enum) log_level);

	auto output_path = vm["output"].as<std::vector<std::string>>()[0];

	auto demuxers = create_demuxers(vm["input"].as<std::vector<std::string>>());
	initialize_demuxers(demuxers);
	auto stream_map = calc_stream_map(demuxers);
	segmentize(demuxers, output_path, stream_map, vm["absolute-time"].as<bool>(), vm["resource-id"].as<std::string>());

	logger->notice("clean exit");
}
