
#pragma once

#include <string>
#include <spdlog/spdlog.h>

template<typename Iter>
std::string dump_bytes(Iter first, Iter last)
{
	std::string ret;

	for(; first != last; ++first)
	{
		ret += fmt::format("{:02x} ", *first);
	}

	return std::move(ret);
}

template<typename Range>
std::string dump_bytes(Range r)
{
	return dump_bytes(begin(r), end(r));
}
