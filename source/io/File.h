
#pragma once

#include <stdexcept>
#include <cstdio>
#include <boost/filesystem.hpp>

class IoException: public std::runtime_error
{
public:
	IoException(std::string msg)
	:
		std::runtime_error(msg)
	{
	}
};

class File
{
	FILE * _fd;
	boost::filesystem::path _path;
public:

	File(boost::filesystem::path path, const char * mode);
	~File();

	void write(const uint8_t * data, size_t size);
	void write(const char * data, size_t size);
	void write(const std::string & s);
	void writeln(const std::string & s);
};
